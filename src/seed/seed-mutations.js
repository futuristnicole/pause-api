export default /* GraphQL */ `
  mutation {
    p1: CreatePeron(id: "p1", name: "Anthony Esposito", description: "Coming Soon") {
      id
      name
    }
    as1: CreateRoll(id: "as1", name: "RANDY", type: "STAR") {
      id
      name
      type
    }
    r1: AddPersonRoll(from: { id: "p1" }, to: {	id: "as1" }) { 
      from {
          id
        }
      }
  }
`;
